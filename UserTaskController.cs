﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using TugasCrud.Models;

namespace TugasCrud.Controllers
{
    public class UserTaskController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public UserTaskController(IConfiguration connection)
        {
            _connection = connection;
        }

        [HttpPost]
        [Route("api/tasks/AddUserWithTask")]
        public string PostUser([FromBody] User inputUser)
        {
            string userQuery = "INSERT INTO Users (name) VALUES (@Name)";

            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand userCmd = new SqlCommand(userQuery, conn);
                userCmd.Parameters.AddWithValue("@Name", inputUser.Name);
                conn.Open();
                userCmd.ExecuteNonQuery();
                conn.Close();
            }

            foreach (var task in inputUser.Tasks)
            {
                string taskQuery = "INSERT INTO Tasks (task_detail, fk_users_id) VALUES (@TaskDetail, (SELECT IDENT_CURRENT('Users')))";
                using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
                {
                    SqlCommand taskCmd = new SqlCommand(taskQuery, conn);
                    taskCmd.Parameters.AddWithValue("@TaskDetail", task.Task_Detail);
                    conn.Open();
                    taskCmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            return "User added successfully";
        }

        [HttpGet]
        [Route("api/tasks/GetUserWithTask")]
        public List<User> GetUserWithTask()
        {
            List<User> userList = new List<User>();
            string query = "SELECT u.pk_users_id, u.name, t.pk_tasks_id, t.task_detail, t.fk_users_id FROM users u JOIN tasks t ON u.pk_users_id = t.fk_users_id";
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    User user = new User();
                    user.Pk_Users_Id = Convert.ToInt32(dt.Rows[i]["pk_users_id"].ToString());
                    user.Name = dt.Rows[i]["name"].ToString();

                    UserTask userTask = new UserTask();
                    userTask.Pk_Tasks_Id = Convert.ToInt32(dt.Rows[i]["pk_tasks_id"].ToString());
                    userTask.Task_Detail = dt.Rows[i]["task_detail"].ToString();
                    userTask.Fk_Users_Id = Convert.ToInt32(dt.Rows[i]["fk_users_id"].ToString());

                    user.Tasks.Add(userTask);

                    if (userList.Exists(x => x.Pk_Users_Id == user.Pk_Users_Id))
                    {
                        userList.Find(x => x.Pk_Users_Id == user.Pk_Users_Id).Tasks.Add(userTask);
                    }
                    else
                    {
                        userList.Add(user);
                    }
                }
            }
            return userList;
        }


        [HttpGet]
        [Route("api/tasks/GetUserWithTaskByName")]
        public List<User> GetUserWithTaskByName(string name)
        {
            List<User> userList = new List<User>();
            string query = "SELECT u.pk_users_id, u.name, t.pk_tasks_id, t.task_detail, t.fk_users_id FROM users u JOIN tasks t ON u.pk_users_id = t.fk_users_id WHERE u.name = @name";
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@name", name);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    User user = new User();
                    user.Pk_Users_Id = Convert.ToInt32(dt.Rows[i]["pk_users_id"].ToString());
                    user.Name = dt.Rows[i]["name"].ToString();

                    UserTask userTask = new UserTask();
                    userTask.Pk_Tasks_Id = Convert.ToInt32(dt.Rows[i]["pk_tasks_id"].ToString());
                    userTask.Task_Detail = dt.Rows[i]["task_detail"].ToString();
                    userTask.Fk_Users_Id = Convert.ToInt32(dt.Rows[i]["fk_users_id"].ToString());

                    user.Tasks.Add(userTask);

                    if (userList.Exists(x => x.Pk_Users_Id == user.Pk_Users_Id))
                    {
                        userList.Find(x => x.Pk_Users_Id == user.Pk_Users_Id).Tasks.Add(userTask);
                    }
                    else
                    {
                        userList.Add(user);
                    }
                }
            }
            return userList;
        }
    }
}
