﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
namespace TugasCrud.Models
{
    public class UserTask
    {
        public int Pk_Tasks_Id { get; set; }
        public string Task_Detail { get; set; }
        public int Fk_Users_Id { get; set; }
    }
}
