﻿using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TugasCrud.Models
{
    public class User
    {
        public List<UserTask> Tasks { get; set; }
        public int Pk_Users_Id { get; set; }
        public string Name { get; set; }

        public User()
        {
            Tasks = new List<UserTask>();
        }
    }
}
